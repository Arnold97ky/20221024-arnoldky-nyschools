//
//  TestScoreView.swift
//  20221024-ArnoldKy-NYCSchools
//
//  Created by Arnold Sylvestre on 10/24/22.
//

import SwiftUI

struct TestScoreView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct TestScoreView_Previews: PreviewProvider {
    static var previews: some View {
        TestScoreView()
    }
}
